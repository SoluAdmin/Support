<?php

namespace SoluAdmin\Support\Tests\Unit\Providers;

use Illuminate\Support\Facades\Route;
use phpmock\mockery\PHPMockery;
use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;
use Mockery as m;
use SoluAdmin\Support\Testing\UnitTestCase;
use Backpack\CRUD\CrudServiceProvider as BackpackCrudServiceProvider;

class CrudServiceProviderTest extends UnitTestCase
{
    protected $allAssetsProvider;

    protected function _before()
    {
        parent::_before();
        $this->allAssetsProvider = new AllAssetsServiceProvider($this->app);
    }

    /** @test */
    public function itRetrievesCurrentDirectory()
    {
        $path = realpath(__DIR__);
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'getDir');
        $this->assertEquals($path, $method->invoke($this->allAssetsProvider));
    }

    /** @test */
    public function itCachesTheDirectory()
    {
        $path = realpath(__DIR__);
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'getDir');

        $this->assertEquals(false, $this->readAttribute($this->allAssetsProvider, 'dirname'));
        $method->invoke($this->allAssetsProvider);
        $this->assertEquals($path, $this->readAttribute($this->allAssetsProvider, 'dirname'));
    }

    /** @test */
    public function itCallsDynamicAssetsBootMethods()
    {
        $assetsMethods = [
            'bootViews',
            'bootSeeds',
            'bootTranslations',
            'bootConfigs',
            'bootModuleMigrations',
            'bootMigrations',
        ];

        $allAssetsStub = m::mock(
            AllAssetsServiceProvider::class
            . '[' . implode(',', $assetsMethods) . ']',
            [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $emptyAssetsStub = m::mock(
            EmptyAssetsServiceProvider::class
            . '[' . implode(',', $assetsMethods) . ']',
            [$this->app])
            ->shouldAllowMockingProtectedMethods();

        foreach ($assetsMethods as $method) {
            $allAssetsStub->shouldReceive($method)->once();
            $emptyAssetsStub->shouldReceive($method)->times(0);
        }

        $allAssetsStub->boot();
        $emptyAssetsStub->boot();
    }

    /** @test */
    public function itAlwaysCallBootExtrasMethod()
    {
        $stub = m::mock(EmptyAssetsServiceProvider::class . '[bootExtras]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $stub->shouldReceive('bootExtras')->once();

        $stub->boot();
    }

    /** @test */
    public function itRegisterRoutesDependingOnConfig()
    {
        $stub = m::mock(AllAssetsServiceProvider::class . '[bootExtras, setupRoutes]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $this->app['config']->set('SoluAdmin.Support.setup_routes', true);
        $stub->shouldReceive('setupRoutes')->once();
        $stub->register();

        $this->app['config']->set('SoluAdmin.Support.setup_routes', false);
        $stub->shouldReceive('setupRoutes')->times(0);
        $stub->register();
    }

    /** @test */
    public function itGeneratesMigrationsFilename()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'getMigrationFileName');
        $this->assertEquals('create_support_tables.php', $method->invoke($this->allAssetsProvider));
    }

    /** @test */
    public function itDeterminesIfMigrationsShouldBePublished()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'shouldPublishMigrations');
        $classExistsMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'class_exists');

        $this->app['config']->set('SoluAdmin.Support.publishes_migrations', false);
        $this->assertEquals(false, $method->invoke($this->allAssetsProvider));

        $this->app['config']->set('SoluAdmin.Support.publishes_migrations', true);

        $classExistsMock->twice()->with("CreateSupportTables")->andReturn(true, false);
        $this->assertEquals(false, $method->invoke($this->allAssetsProvider));
        $this->assertEquals(true, $method->invoke($this->allAssetsProvider));
    }

    /** @test */
    public function itDeterminesIfModuleMigrationsShouldBePublished()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'shouldPublishModuleMigrations');
        $classExistsMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'class_exists');
        $module = "SoluAdmin/Support";

        $this->app['config']->set('SoluAdmin.Support.publishes_migrations', false);
        $this->assertEquals(false, $method->invokeArgs($this->allAssetsProvider, [$module]));

        $this->app['config']->set('SoluAdmin.Support.publishes_migrations', true);
        $classExistsMock->twice()->with("SoluAdminCreateSupportTables")->andReturn(true, false);
        $this->assertEquals(false, $method->invokeArgs($this->allAssetsProvider, [$module]));
        $this->assertEquals(true, $method->invokeArgs($this->allAssetsProvider, [$module]));
    }

    /** @test */
    public function itRetrievesTheModules()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'getModules');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $globMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'glob');
        $modules = [
            "/var/www/app/../../database/modules/SoluAdmin/Support",
            "/var/www/app/../../database/modules/SoluAdmin/Testing",
        ];

        $stub->shouldReceive('getDir')->times(3)->andReturn('/var/www/app');
        $globMock->once()->with('/var/www/app/../../database/modules/*/*', GLOB_ONLYDIR)->andReturn($modules);

        $this->assertEquals(['SoluAdmin/Support', 'SoluAdmin/Testing'], $method->invoke($stub));
    }

    /** @test */
    public function itPublishesMigrations()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'publishMigrations');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir, publishes]', [$this->app])
            ->shouldAllowMockingProtectedMethods();
        $timeMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'time');
        $dateMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'date');

        $time = time();
        $timestamp = date('Y_m_d_His', $time);
        $fileName = 'create_support_tables.php';
        $destination = $this->app->databasePath() . "/migrations/{$timestamp}_{$fileName}";
        $stubLocation = "/var/www/app/../../database/migrations/{$fileName}.stub";


        $timeMock->once()->andReturn($time);
        $dateMock->once()->with('Y_m_d_His', $time)->andReturn($timestamp);


        $stub->shouldReceive('getDir')->once()->andReturn('/var/www/app');
        $stub->shouldReceive('publishes')->once()->with(
            [
                $stubLocation => $destination,
            ],
            'migrations');

        $method->invoke($stub);
    }

    /** @test */
    public function itPublishesModuleMigrations()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'publishModuleMigration');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir, publishes]', [$this->app])
            ->shouldAllowMockingProtectedMethods();
        $timeMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'time');
        $dateMock = PHPMockery::mock('SoluAdmin\Support\Providers', 'date');

        $time = time();
        $module = 'SoluAdmin/Support';
        $timestamp = date('Y_m_d_His', $time);
        $fileName = 'create_support_tables.php';
        $destination = $this->app->databasePath() . "/modules/{$module}/{$timestamp}_solu_admin_{$fileName}";
        $stubLocation = "/var/www/app/../../database/modules/{$module}/solu_admin_{$fileName}.stub";


        $timeMock->once()->andReturn($time);
        $dateMock->once()->with('Y_m_d_His', $time)->andReturn($timestamp);


        $stub->shouldReceive('getDir')->once()->andReturn('/var/www/app');
        $stub->shouldReceive('publishes')->once()->with(
            [
                $stubLocation => $destination,
            ],
            'migrations-modules');

        $method->invokeArgs($stub, [$module]);
    }

    /** @test */
    public function itBootSeeds()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'bootSeeds');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir, publishes]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $stub->shouldReceive('getDir')->once()->andReturn('/var/www/app');
        $stub->shouldReceive('publishes')->once()->with(
            [
                '/var/www/app/../../database/seeds/' => $this->app->databasePath('seeds'),
            ],
            'seeds'
        );

        $method->invoke($stub);
    }

    /** @test */
    public function itBootViews()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'bootViews');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir, publishes, loadViewsFrom]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $stub->shouldReceive('getDir')->twice()->andReturn('/var/www/app');
        $stub->shouldReceive('loadViewsFrom')->once()->with('/var/www/app/../../resources/views', 'SoluAdmin');
        $stub->shouldReceive('publishes')->once()->with(
            [
                '/var/www/app/../../resources/views' => $this->app->resourcePath('views/vendor/SoluAdmin/'),
            ],
            'views'
        );

        $method->invoke($stub);
    }

    /** @test */
    public function itBootTranslations()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'bootTranslations');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir, publishes, loadTranslationsFrom]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $stub->shouldReceive('getDir')->twice()->andReturn('/var/www/app');
        $stub->shouldReceive('loadTranslationsFrom')->once()->with('/var/www/app/../../resources/lang', 'SoluAdmin');
        $stub->shouldReceive('publishes')->once()->with(
            [
                '/var/www/app/../../resources/lang' => $this->app->resourcePath('lang/vendor/SoluAdmin'),
            ],
            'lang'
        );

        $method->invoke($stub);
    }

    /** @test */
    public function itBootConfigs()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'bootConfigs');
        $stub = m::mock(AllAssetsServiceProvider::class . '[getDir, publishes, mergeConfigFrom]', [$this->app])
            ->shouldAllowMockingProtectedMethods();

        $stub->shouldReceive('getDir')->twice()->andReturn('/var/www/app');
        $stub->shouldReceive('mergeConfigFrom')
            ->once()
            ->with('/var/www/app/../../config/SoluAdmin/Support.php', 'SoluAdmin/Support');
        $stub->shouldReceive('publishes')->once()->with(
            [
                '/var/www/app/../../config/' => $this->app->configPath(),
            ],
            'config'
        );

        $method->invoke($stub);
    }

    /** @test */
    public function itBootMigrations()
    {
        $mockedMethods = AllAssetsServiceProvider::class . '[shouldPublishMigrations, publishMigrations]';
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'bootMigrations');
        $stub = m::mock($mockedMethods, [$this->app])->shouldAllowMockingProtectedMethods();

        $stubWithoutMigrations = m::mock($mockedMethods, [$this->app])->shouldAllowMockingProtectedMethods();

        $stub->shouldReceive('shouldPublishMigrations')->once()->andReturn(true);
        $stub->shouldReceive('publishMigrations')->once();

        $stubWithoutMigrations->shouldReceive('shouldPublishMigrations')->once()->andReturn(false);
        $stubWithoutMigrations->shouldReceive('publishMigrations')->times(0);

        $method->invoke($stub);
        $method->invoke($stubWithoutMigrations);
    }

    /** @test */
    public function itBootModuleMigrations()
    {
        $mockedMethods = AllAssetsServiceProvider::class . '[shouldPublishModuleMigrations, publishModuleMigration, getModules]';
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'bootModuleMigrations');
        $stubWithoutMigrations = m::mock($mockedMethods, [$this->app])->shouldAllowMockingProtectedMethods();
        $stubWithoutModules = m::mock($mockedMethods, [$this->app])->shouldAllowMockingProtectedMethods();
        $stub = m::mock($mockedMethods, [$this->app])->shouldAllowMockingProtectedMethods();

        $stubWithoutModules->shouldReceive('getModules')->once()->andReturn([]);
        $method->invoke($stubWithoutModules);

        $stubWithoutMigrations->shouldReceive('getModules')->once()->andReturn([
            'SoluAdmin/Support',
            'SoluAdmin/Testing'
        ]);
        $stubWithoutMigrations->shouldReceive('shouldPublishModuleMigrations')
            ->once()
            ->with('SoluAdmin/Support')
            ->andReturn(false);
        $stubWithoutMigrations->shouldReceive('shouldPublishModuleMigrations')
            ->once()
            ->with('SoluAdmin/Testing')
            ->andReturn(false);
        $method->invoke($stubWithoutMigrations);

        $stub->shouldReceive('getModules')->once()->andReturn(['SoluAdmin/Support', 'SoluAdmin/Testing']);
        $stub->shouldReceive('shouldPublishModuleMigrations')
            ->once()
            ->with('SoluAdmin/Support')
            ->andReturn(true);
        $stub->shouldReceive('shouldPublishModuleMigrations')
            ->once()
            ->with('SoluAdmin/Testing')
            ->andReturn(true);
        $stub->shouldReceive('publishModuleMigration')->once()->with('SoluAdmin/Support');
        $stub->shouldReceive('publishModuleMigration')->once()->with('SoluAdmin/Testing');
        $method->invoke($stub);
    }

    /** @test */
    public function itCanHaveMiddlewaresConfigured()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'getControllersMiddleware');

        $this->app['config']->set('SoluAdmin.Support.middleware', false);
        $this->assertEquals(['web', 'admin'], $method->invoke($this->allAssetsProvider));

        $this->app['config']->set('SoluAdmin.Support.middleware', ['permission:foo', 'role:bar']);
        $this->assertEquals(['web', 'admin', 'permission:foo', 'role:bar'], $method->invoke($this->allAssetsProvider));
    }

    /** @test */
    public function setupRoutes()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'setupRoutes');

        $routeMock = m::mock('alias:' . Route::class)->shouldAllowMockingProtectedMethods();

        $routeMock->shouldReceive('group')
            ->once()
            ->with(
                [
                    'namespace' => "SoluAdmin\\Support\\Http\\Controllers",
                    'prefix' => 'admin',
                    'middleware' => ['web', 'admin'],
                ],
                m::type('Closure')
            );

        $method->invoke($this->allAssetsProvider);
    }

    /** @test */
    public function setupResourceRoutes()
    {
        $method = $this->getPrivateMethod(AllAssetsServiceProvider::class, 'setupResourceRoutes');

        $crudMock = m::mock('alias:' . BackpackCrudServiceProvider::class)->shouldAllowMockingProtectedMethods();

        $crudMock->shouldReceive('resource')->once()->with('support', 'SupportCrudController');
        $crudMock->shouldReceive('resource')->once()->with('testing', 'TestingCrudController');

        $method->invoke($this->allAssetsProvider);
    }
}

class AllAssetsServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::VIEWS,
        Assets::SEEDS,
        Assets::TRANSLATIONS,
        Assets::CONFIGS,
        Assets::MODULE_MIGRATIONS,
        Assets::MIGRATIONS,
    ];

    protected $packageName = "Support";
    protected $packageVendor = "SoluAdmin";

    protected $resources = [
        'support' => 'SupportCrudController',
        'testing' => 'TestingCrudController',
    ];
}

class EmptyAssetsServiceProvider extends CrudServiceProvider
{
    protected $assets = [];
}