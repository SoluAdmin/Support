<?php

namespace SoluAdmin\Support\Tests\Unit\Providers;

use Illuminate\Container\Container;
use Illuminate\Foundation\Application;
use SoluAdmin\Support\Providers\SupportServiceProvider;
use Mockery as m;
use SoluAdmin\Support\Testing\UnitTestCase;

class SupportServiceProviderTest extends UnitTestCase
{
    protected $app;
    protected $mockedApp;
    protected $stub;

    protected function _before()
    {
        parent::_before();
        $this->mockedApp = m::mock(Application::class);
        $this->stub = m::mock(SupportServiceProvider::class . '[mergeConfigFrom,publishes]', [$this->mockedApp])
            ->shouldAllowMockingProtectedMethods();
        Container::setInstance($this->mockedApp);
    }

    /** @test */
    public function serviceProviderIsNotDefered()
    {
        $this->assertTrue(!$this->stub->isDeferred());
    }

    /** @test  */
    public function itRegisterDependencies()
    {
        $this->mockedApp->shouldReceive('register')
            ->once()
            ->with(\Felixkiss\UniqueWithValidator\ServiceProvider::class)
            ->andReturnNull();

        $this->assertNull($this->stub->register());
    }

    /** ignored till it is properly moced test */
    public function bootMethodUsesProtectedMethodBootConfigs()
    {
        $this->stub = m::mock(SupportServiceProvider::class . '[bootConfigs]', [$this->mockedApp])
            ->shouldAllowMockingProtectedMethods();

        $this->stub->shouldReceive('bootConfigs')
            ->once()
            ->andReturnNull();

        $this->assertNull($this->stub->boot());
    }

    /** ignored till it is properly moced test */
    public function itPublishesAndMergesConfigurations()
    {
        $configPath = realpath(__DIR__ . '/../../../resources/config/');

        $this->mockedApp->shouldReceive('make')->once()->with('path.config')->andReturn('/var/www/config');

        $this->stub->shouldReceive('mergeConfigFrom')->once()
            ->with($configPath . '/SoluAdmin/Support.php', 'SoluAdmin/Support')
            ->andReturnNull();

        $this->stub->shouldReceive('publishes')
            ->with([$configPath => '/var/www/config'], 'config')
            ->andReturnNull();

        $this->assertNull($this->stub->boot());
    }
}