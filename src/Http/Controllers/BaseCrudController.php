<?php

namespace SoluAdmin\Support\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use SoluAdmin\Support\Traits\ResolvesPackageName;

abstract class BaseCrudController extends CrudController
{
    use ResolvesPackageName;

    private $reflector;

    public function __construct()
    {
        parent::__construct();
        $this->reflector = new \ReflectionClass($this);
    }

    public function model()
    {
        $model = str_replace('CrudController', '', $this->reflector->getShortName());
        return "{$this->getPackageVendor()}\\{$this->getPackageName()}\\Models\\{$model}";
    }

    public function entityKey()
    {
        return str_slug(str_replace('CrudController', '', $this->reflector->getShortName()));
    }

    public function route()
    {
        return $this->entityKey();
    }

    public function form()
    {
        $form = str_replace('Controller', '', $this->reflector->getShortName()) . 'Form';
        $formClass = "{$this->getPackageVendor()}\\{$this->getPackageName()}\\Http\\Forms\\{$form}";
        return new $formClass;
    }

    public function dataTable()
    {
        $dataTable = str_replace('Controller', '', $this->reflector->getShortName()) . 'DataTable';
        $dataTableClass = "{$this->getPackageVendor()}\\{$this->getPackageName()}\\Http\\DataTables\\{$dataTable}";
        return new $dataTableClass;
    }

    public function setup()
    {
        $this->crud->setModel($this->model());
        $this->crud->setEntityNameStrings(
            trans("{$this->getPackageVendor()}::{$this->getPackageName()}.{$this->entityKey()}_singular"),
            trans("{$this->getPackageVendor()}::{$this->getPackageName()}.{$this->entityKey()}_plural")
        );

        $this->crud->setRoute(
            config('backpack.base.route_prefix', 'admin')
            . config("{$this->getPackageVendor()}.{$this->getPackageName()}.route_prefix", '')
            . "/{$this->route()}"
        );

        $this->crud->addFields($this->form()->fields());
        $this->crud->addColumns($this->dataTable()->columns());
        parent::setup();
    }
}
