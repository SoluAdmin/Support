<?php

namespace SoluAdmin\Support\Testing;

use Codeception\Test\Unit;
use SoluAdmin\Support\Testing\Traits\TestHelperTrait;
use Mockery as m;

class UnitTestCase extends Unit
{
    use TestHelperTrait;

    // @codingStandardsIgnoreStart
    protected function _before()
    {
        parent::_before();

        $this->startApplication();
    }

    protected function _after()
    {
        $this->stopApplication();
        m::close();
        parent::_after();
    }
    // @codingStandardsIgnoreEnd
}
