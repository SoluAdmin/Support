<?php

namespace SoluAdmin\Support\Testing\Traits;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Facade;
use Orchestra\Testbench\Traits\CreatesApplication;
use SoluAdmin\Support\Testing\Exceptions\ApplicationRunningException;

trait ApplicationInitiatorTrait
{
    use CreatesApplication;

    /**
     * @var Application
     */
    protected $app = null;

    /**
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * @var \Illuminate\Database\Eloquent\Factory
     */
    protected $factory;

    /**
     * @var array
     */
    protected $beforeApplicationDestroyedCallbacks = [];

    /**
     * @throws ApplicationRunningException If an application has already been started / initialised and running
     */
    public function startApplication()
    {
        if ($this->hasApplicationBeenStarted()) {
            $msg = sprintf('Application has already been started. Please stop the application, before invoking start!');
            throw new ApplicationRunningException($msg);
        }

        $this->refreshApplication();

        if (!$this->factory) {
            $this->factory = $this->app->make(Factory::class);
        }
    }

    public function stopApplication()
    {
        if ($this->hasApplicationBeenStarted()) {
            foreach ($this->beforeApplicationDestroyedCallbacks as $callback) {
                call_user_func($callback);
            }

            $this->app->flush();
            $this->app = null;

            Facade::clearResolvedInstances();
            Facade::setFacadeApplication(null);
        }
    }

    protected function refreshApplication()
    {
        putenv('APP_ENV=testing');

        $this->app = $this->createApplication();
    }

    /**
     * @return \Illuminate\Foundation\Application|null Instance of the application Or null if none has been started
     */
    public function getApplication()
    {
        return $this->app;
    }

    public function hasApplicationBeenStarted()
    {
        if (!is_null($this->app)) {
            return true;
        }
        return false;
    }

    protected function getEnvironmentSetUp($app)
    {
        // Define your environment setup.
    }

    protected function beforeApplicationDestroyed(callable $callback)
    {
        $this->beforeApplicationDestroyedCallbacks[] = $callback;
    }
}
