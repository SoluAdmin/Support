<?php

namespace SoluAdmin\Support\Testing\Traits;

use ReflectionClass;
use ReflectionMethod;

trait ReadsPrivateMethods
{

    /**
     * getPrivateMethod
     *
     * @param    string $className
     * @param    string $methodName
     * @return    ReflectionMethod
     */
    public function getPrivateMethod($className, $methodName)
    {
        $reflector = new ReflectionClass($className);
        $method = $reflector->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}
