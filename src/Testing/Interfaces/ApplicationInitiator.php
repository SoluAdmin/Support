<?php

namespace SoluAdmin\Support\Testing\Interfaces;

use SoluAdmin\Support\Testing\Exceptions\ApplicationRunningException;

interface ApplicationInitiator
{

    /**
     * Start the application
     *
     * @return void
     *
     * @throws ApplicationRunningException If an application has already been started / initialised and running
     */
    public function startApplication();

    /**
     * Stop the application
     *
     * @return void
     */
    public function stopApplication();

    /**
     * Get the application instance
     *
     * @return \Illuminate\Foundation\Application|null Instance of the application Or null if none has been started
     */
    public function getApplication();

    /**
     * Check if the application has been started
     *
     * @return bool True if an application instance has been created, initialised and running. False if not.
     */
    public function hasApplicationBeenStarted();
}
