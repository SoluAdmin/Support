<?php

namespace SoluAdmin\Support\Testing\Interfaces;

use Orchestra\Testbench\Contracts\TestCase as TestCaseContract;

interface TestHelper extends IApplicationInitiator, TestCaseContract
{

}
