<?php

namespace SoluAdmin\Support\Testing\Exceptions;

use RuntimeException;

class ApplicationRunningException extends RuntimeException
{
}
