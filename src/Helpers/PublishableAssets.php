<?php


namespace SoluAdmin\Support\Helpers;

abstract class PublishableAssets
{
    const VIEWS = 'VIEWS';
    const TRANSLATIONS = 'TRANSLATIONS';
    const CONFIGS = 'CONFIGS';
    const MIGRATIONS = 'MIGRATIONS';
    const MODULE_MIGRATIONS = 'MODULE_MIGRATIONS';
    const SEEDS = 'SEEDS';
}
