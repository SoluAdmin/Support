<?php

namespace SoluAdmin\Support\Providers;

use Backpack\CRUD\CrudServiceProvider as BackpackCrudServiceProvider;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use SoluAdmin\Support\Traits\ResolvesPackageName;

abstract class CrudServiceProvider extends ServiceProvider
{
    use ResolvesPackageName;

    protected $defer = false;

    protected $assets = [];

    protected $resources = [];

    protected $dirname = false;

    public function boot()
    {
        array_walk($this->assets, function ($asset) {
            $this->{'boot' . ucfirst(camel_case(strtolower($asset)))}();
        });

        $this->bootExtras();
    }

    public function register()
    {
        if (config("{$this->getPackageVendor()}.{$this->getPackageName()}.setup_routes")) {
            $this->setupRoutes();
        }

        $this->registerExtras();
    }

    protected function getMigrationFileName()
    {
        return "create_" . snake_case($this->getPackageName()) . "_tables.php";
    }

    protected function bootExtras()
    {
    }

    protected function registerExtras()
    {
    }

    protected function bootMigrations()
    {
        if ($this->shouldPublishMigrations()) {
            $this->publishMigrations();
        }
    }

    protected function bootSeeds()
    {
        $this->publishes([$this->getDir() . '/../../database/seeds/' => database_path('seeds')], 'seeds');
    }

    protected function bootModuleMigrations()
    {
        $modules = $this->getModules();

        array_walk($modules, function ($module) {
            if ($this->shouldPublishModuleMigrations($module)) {
                $this->publishModuleMigration($module);
            }
        });
    }

    protected function bootTranslations()
    {
        $this->loadTranslationsFrom($this->getDir() . '/../../resources/lang', $this->getPackageVendor());
        $this->publishes(
            [
                $this->getDir() . '/../../resources/lang' => resource_path("lang/vendor/{$this->getPackageVendor()}")
            ],
            'lang'
        );
    }

    protected function bootViews()
    {
        $this->loadViewsFrom($this->getDir() . '/../../resources/views', $this->getPackageVendor());
        $this->publishes([
            $this->getDir() . '/../../resources/views' => resource_path("views/vendor/{$this->getPackageVendor()}/"),
        ], 'views');
    }

    protected function getDir()
    {
        if (!$this->dirname) {
            $this->dirname = dirname((new \ReflectionClass($this))->getFileName());
        }

        return $this->dirname;
    }

    protected function getModules()
    {
        $modulesFolder = glob($this->getDir() . "/../../database/modules/*/*", GLOB_ONLYDIR);

        return array_map(function ($folder) {
            return str_replace($this->getDir() . "/../../database/modules/", "", $folder);
        }, $modulesFolder);
    }

    protected function bootConfigs()
    {
        $this->mergeConfigFrom(
            $this->getDir() . "/../../config/{$this->getPackageVendor()}/{$this->getPackageName()}.php",
            "{$this->getPackageVendor()}/{$this->getPackageName()}"
        );

        $this->publishes([$this->getDir() . '/../../config/' => config_path()], 'config');
    }

    protected function publishMigrations()
    {
        $fileName = $this->getMigrationFileName();
        $timestamp = date('Y_m_d_His', time());

        $destination = $this->app->databasePath() . "/migrations/{$timestamp}_{$fileName}";

        $stubLocation = $this->getDir() . "/../../database/migrations/{$fileName}.stub";

        $this->publishes([
            $stubLocation => $destination
        ], 'migrations');
    }

    protected function publishModuleMigration($module)
    {
        $fileName = $this->getMigrationFileName();
        $timestamp = date('Y_m_d_His', time());
        $parts = explode('/', $module);
        $vendor = snake_case($parts[0]);

        $destination = $this->app->databasePath() . "/modules/{$module}/{$timestamp}_{$vendor}_{$fileName}";

        $stubLocation = $this->getDir()
            . "/../../database/modules/{$module}/{$vendor}_{$fileName}.stub";

        $this->publishes([
            $stubLocation => $destination,
        ], 'migrations-modules');
    }

    protected function getControllersNamespace()
    {
        return "{$this->getPackageVendor()}\\{$this->getPackageName()}\\Http\\Controllers";
    }

    protected function getControllersPrefix()
    {
        return config('backpack.base.route_prefix', 'admin')
            . config("{$this->getPackageVendor()}.{$this->getPackageName()}.route_prefix");
    }

    protected function getControllersMiddleware()
    {
        return config("{$this->getPackageVendor()}.{$this->getPackageName()}.middleware")
            ? array_merge(['web', 'admin'], config("{$this->getPackageVendor()}.{$this->getPackageName()}.middleware"))
            : ['web', 'admin'];
    }

    protected function setupRoutes()
    {
        Route::group(
            [
                'namespace' => $this->getControllersNamespace(),
                'prefix' => $this->getControllersPrefix(),
                'middleware' => $this->getControllersMiddleware(),
            ],
            function () {
                $this->setupResourceRoutes();
            }
        );
    }

    protected function setupResourceRoutes()
    {
        array_walk($this->resources, function ($value, $key) {
            BackpackCrudServiceProvider::resource($key, $value);
        });
    }

    protected function shouldPublishModuleMigrations($module)
    {
        if (!config("{$this->getPackageVendor()}.{$this->getPackageName()}.publishes_migrations")) {
            return false;
        }

        $parts = explode('/', $module);
        return !class_exists("{$parts[0]}Create{$parts[1]}Tables");
    }

    protected function shouldPublishMigrations()
    {
        if (!config("{$this->getPackageVendor()}.{$this->getPackageName()}.publishes_migrations")) {
            return false;
        }

        return !class_exists("Create{$this->getPackageName()}Tables");
    }
}
