<?php

namespace SoluAdmin\Support\Providers;

use Illuminate\Support\ServiceProvider;

class SupportServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->app->register(\Felixkiss\UniqueWithValidator\ServiceProvider::class);
    }

    public function boot()
    {
        $this->bootConfigs();
        $this->bootTranslations();
    }

    protected function bootConfigs()
    {
        $this->mergeConfigFrom(
            realpath(__DIR__ . "/../../resources/config/SoluAdmin/Support.php"),
            "SoluAdmin/Support"
        );

        $this->publishes([realpath(__DIR__ . '/../../resources/config/') => config_path()], 'config');
    }

    protected function bootTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'SoluAdmin');
        $this->publishes(
            [
                __DIR__ . '/../../resources/lang' => resource_path('lang/vendor/SoluAdmin')
            ],
            'lang'
        );
    }
}
