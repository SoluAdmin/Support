<?php

namespace SoluAdmin\Support\Interfaces;

interface Form
{
    public function fields();
}
