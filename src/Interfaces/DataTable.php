<?php

namespace SoluAdmin\Support\Interfaces;

interface DataTable
{
    public function columns();
}
