<?php


namespace SoluAdmin\Support\Traits;

trait ResolvesPackageName
{
    protected $packageName = false;
    protected $packageVendor = false;

    protected function getPackageName()
    {
        if (!$this->packageName) {
            $this->packageName = $this->getNamespaceSegment(1);
        }

        return $this->packageName;
    }

    protected function getPackageVendor()
    {
        if (!$this->packageVendor) {
            $this->packageVendor = $this->getNamespaceSegment(0);
        }

        return $this->packageVendor;
    }

    protected function getNamespaceSegment($position)
    {
        $matches = [];
        preg_match_all('/(.*?)\\\\/', (new \ReflectionClass($this))->getNamespaceName(), $matches);
        return $matches[1][$position];
    }
}
