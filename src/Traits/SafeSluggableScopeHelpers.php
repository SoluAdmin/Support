<?php

namespace SoluAdmin\Support\Traits;

use Backpack\CRUD\ModelTraits\SpatieTranslatable\SluggableScopeHelpers as OriginalSluggableScopeHelpers;

trait SafeSluggableScopeHelpers
{
    use OriginalSluggableScopeHelpers;

    public function scopeWhereSlug($scope, $slug)
    {

        return config('SoluAdmin.Support.json_columns_support')
            ? $scope->where($this->getSlugKeyName().'->'.$this->getLocale(), $slug)
            : $scope->where($this->getSlugKeyName(), 'regexp', "\"{$this->getLocale()}\"\s*:\s*\"{$slug}\"");
    }
}
