<?php

namespace SoluAdmin\Support\Traits;

trait HasItemsButton
{
    abstract public function getItemEndpoint();

    public function showItemsButton()
    {
        $itemUrl = \Request::url() . '/' . $this->id . "/" . $this->getItemEndpoint();
        $label = trans('SoluAdmin::Support.see_items');

        return <<< EOD
        <a href="$itemUrl" class="btn btn-xs btn-default">
            <i class="fa fa-eye"></i>
            $label
        </a>
EOD;
    }
}
