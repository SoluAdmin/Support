<?php

namespace SoluAdmin\Support\Traits;

use Illuminate\Database\Eloquent\Builder;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\Sluggable as OriginalSluggable;

trait SafeSluggable
{
    use OriginalSluggable;

    public function scopeFindSimilarSlugs(Builder $query, $attribute, $config, $slug)
    {
        $separator = $config['separator'];
        $textAttribute = "{\"" . $this->getLocale() . "\":\"$attribute\"}";
        $attribute = $attribute . '->' . $this->getLocale();

        return $query->where(function (Builder $q) use ($attribute, $slug, $separator, $textAttribute) {
            $q->where($attribute, '=', $slug)
                ->orWhere($attribute, 'LIKE', $slug . $separator . '%')
                ->orWhere('slug', 'regexp', "\"{$this->getLocale()}\"\s*:\s*\"{$slug}");
        });
    }
}
